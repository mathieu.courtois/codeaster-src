#!/bin/bash

jobs=$(( $(nproc) * 7 / 8 ))
args=( "--clean" "--timefactor=4.0" "--jobs=${jobs}" "$@" )
# to be directly readable in the browser
export MESS_EXT="mess.txt"

printf "\nrunning testcases #1... - $(date)\n"
./install/bin/run_ctest "${args[@]}"
iret=$?

if [ ${iret} -ne 0 ]; then
    printf "\nrunning testcases #2 (rerun-failed)... - $(date)\n"
    ./install/bin/run_ctest "${args[@]}" --rerun-failed
    iret=$?
fi

if [ ${iret} -ne 0 ]; then
    printf "\nrunning testcases #3 (rerun-failed)... - $(date)\n"
    ./install/bin/run_ctest "${args[@]}" --rerun-failed
    iret=$?
fi

exit ${iret}
